<?php
use Symfony\Component\Yaml\Exception\ExceptionInterface;
class ts3v extends ts3admin {
	function ts3clientsConnected() {
		$this->clientList ();
	}
	function findUser() {
		$user = $_POST ['username'];
		$querry = $this->clientDbFind ( $user );
		
		if (isset ( $querry )) {
			return $querry;
		} else {
			Throw new ExceptionInterface ();
		}
	}
	function delUser() {
		$user = $_POST ['username'];
		$querry = $this->delUser ( $user );
		
		if (isset ( $querry )) {
			return $querry;
		} else {
			throw new ExceptionInterface ();
		}
	}
}
interface ts3vInterface extends ts3v {
	public function ts3clientsConnected();
	public function findUser();
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see ts3v::delUser()
	 */
	public function delUser();
}