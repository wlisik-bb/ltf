<?php

class versioningClass {

	public $phpVer, $scriptVer, $verDet;

	function phpVersion() {
		$this->phpVer = PHP_VERSION;
	}

	function scriptVer() {
		$this->scriptVer = "0.1";
	}

	function getLatestVersions() {
		$this->verDet = array(
				"PHP version" => $this->phpVer,
				"LTF version" => $this->scriptVer,
		);

		print_r($this->verDet);
	}
}