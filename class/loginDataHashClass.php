<?php

/*
 * Usage:
 * 
 * include '../class/loginDataHashClass.php';
 * $ldhc = new loginDataHashClass();
 * $ldhc->loginDataHash();
 * 
 */

class loginDataHashClass {
	
	public $pwd, $hashedPwd, $login, $loginHashed;
	
	function loginDataHash() {
		$this->pwd = 'pwd123';
		$this->login = 'admin';
		$this->hashedPwd = password_hash($this->pwd, PASSWORD_BCRYPT, array('cost' => 14));
		$this->loginHashed = password_hash($this->login, PASSWORD_BCRYPT, array('cost' => 14));
	}
}