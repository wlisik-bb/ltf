<?php

class Math {
	
	public $a, $b, $c, $d, $sum, $subs, $multi, $div;
	
	function assignValues($param) {
		$this->a = rand(10, 200);
		$this->b = rand(10, 200);
		$this->c = rand(10, 200);
		$this->d = rand(10, 200);
	}
	
	function sum($a, $b) {
		$this->sum = $this->a + $this->b;
		return $this->sum;
	}
	
	function substract($a, $b) {
		$this->subs = $b - $a;
		return $this->subs;
	}
	
	function multiply($a, $b) {
		$this->multi = $a * $b;
		return $this->multi;
	}
	
	function division($a, $b) {
		$this->div = $b / $a;
		return $this->div;
	}
	
	function loadedFile() {
		$parts = pathinfo($_SERVER['SCRIPT_FILENAME']);
		echo $parts['dirname'].'<br>';
		echo $parts['filename'].'<br>';
		echo $parts['extension'];
	}
}