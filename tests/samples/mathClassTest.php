<?php

class mathClassTests extends Math implements mathClassTests_Interface {
	
/* 	public $variables = array(
			[1] => $this->b,
			[2] => $this->c,
			[3] => $this->d,
	); */
	
	public $classFile = 'tests/mathClassTest.php';
	
	
	protected $m;
	
	protected function testClassCreation() {
		$this->m = new Math();
	}
	
	protected function testVariablesCreation() {
		if ($this->variables){
			return $this->variables;
		} else {
			return FALSE;
		}
	}
	
	protected function assertVariablesAboveZero() {
		foreach ($this->variables as $value) {
			echo $value;
		}
	}
	
	protected function variablesAreInt($a, $b, $c, $d) {
		foreach ($this->variables as $key => $value) {
			gettype($key);
		}
	}
}

interface mathClassTests_Interface {
	abstract protected function testClassCreation($param) {}
	abstract protected function testVariablesCreation(){}
	abstract protected function assertVariablesAboveZero(){}
	abstract protected function variablesAreInt(){}
}