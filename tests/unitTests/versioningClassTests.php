<?php

class versioningClass_Test extends versioningClass {

	function versioningClassExistTest() {
		if (class_exists(versioningClass)) {
			echo 'Fuction OK';
		} else {
			die("No main class found");
		}
	}

	function versioningClass_TestExist() {
		if (class_exists(versioningClass_Test)) {
			echo 'Fuction OK';
		} else {
			die("No test class found");
		}
	}

	function phpVersionFunctionExistTest() {
		if (function_exists($this->phpVersion())) {
			echo 'Fuction OK';
		} else {
			echo 'No (or broken) function';
		}
	}

	function scriptVerFuctionExistTest() {
		if (function_exists($this->scriptVer())) {
			echo 'Function OK';
		} else {
			echo 'No (or broken) function';
		}
	}

	function getLatestVersionsExistTest() {
		if (function_exists($this->getLatestVersions())) {
			echo 'Function OK';
		} else {
			echo 'No (or broken) function';
		}
	}

	function isVerDetArrayTest() {
		if (is_array($this->verDet)) {
			echo 'This is an array'.'<br>';
			print_r($this->verDet);
		} else {
			echo "This is not an array !!!";
			die();
		}
	}

	function assertPHPVerValueTest() {
		/* assert_options(ASSERT_ACTIVE, 1);
		 assert_options(ASSERT_QUIET_EVAL, 1);
		 $assertion = assert(PHP_VERSION, '>= 5.6'); */
		$txt = "Assertion fails for some reason";
		echo $txt;
	}
}